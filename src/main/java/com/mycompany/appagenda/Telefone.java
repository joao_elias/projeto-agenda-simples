package com.mycompany.appagenda;

/**
 *
 * @author joao_elias
 */
public class Telefone {
    private String numero, tipo;

    public Telefone(String numero, String tipo) {
        this.numero = numero;
        this.tipo = tipo;
    }
    
    public String getTelefone(){
        return numero+" "+tipo;
    }
}
