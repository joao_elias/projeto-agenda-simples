package com.mycompany.appagenda;

import java.util.GregorianCalendar;

/**
 *
 * @author joao_elias
 */
public class AppAgenda {
    
    static void mostraDados(ContatoBasico obj)
    {  System.out.println(obj.getDados());
        if(obj instanceof Contato)
          System.out.println(((Contato)obj).getIdade());
          System.out.println("xxxxxxxxxxxxxxxxxxxxxxxxxx");
    }

    public static void main(String[] args) {
        Agenda agenda = new Agenda();
        
        Contato cb;
           cb=new Contato("Joao Elias Ferraz", new GregorianCalendar(1988,05,03));
           cb.setTelefone(new Telefone("3145-2469","Telefone residencial"));
           cb.setTelefone(new Telefone("99958-0275","Telefone celular"));
        
        agenda.inserir(cb);
        
        ContatoComercial cc = new ContatoComercial("Colegio Antonio Vieira", "Educacao", "Leonardo Mendes");
           cc.setTelefone(new Telefone("3328-9500","Telefone residencial"));
        
        agenda.inserir(cc);
        
        agenda.inserir(new EContato("Giovanna Rosangela Rezende", new GregorianCalendar(1975,2,11), "giovanna_rosangela_rezende@com.br","www.4devs.com.br"));
        
        ContatoBasico objeto = agenda.buscar("Joao Elias Ferraz");
        
        if(objeto != null)
            mostraDados(objeto); //encontrou
        else
            System.out.println("Contato nao encontrado!");
    }
}
